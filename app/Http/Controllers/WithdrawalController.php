<?php

namespace App\Http\Controllers;

use App\Account;
use App\Rules\EnoughFunds;
use App\Transaction;
use Illuminate\Http\Request;

class WithdrawalController extends Controller
{
    public function store(Request $request)
    {
        $validatedRequest = $request->validate([
            'account_number' => [
                'required',
                'regex:/^[0-9]{26}$/',
                'exists:accounts,number,deleted_at,NULL'
            ],
            'amount'         => [
                'required',
                'numeric',
                'between:0.01,999999999999.99',
                new EnoughFunds($request->get('account_number', ''))
            ],
        ]);

        $account = Account::find($validatedRequest['account_number']);

        $transaction = new Transaction();
        $transaction->source_account_number = $validatedRequest['account_number'];
        $transaction->amount = round($validatedRequest['amount'] * -1, 2);
        $transaction->title = '';
        $transaction->balance_after = round($account->balance - $validatedRequest['amount'], 2);
        $transaction->save();
    }
}
