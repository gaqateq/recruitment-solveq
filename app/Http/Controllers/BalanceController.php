<?php

namespace App\Http\Controllers;

use App\Account;
use Illuminate\Http\Request;

class BalanceController extends Controller
{
    public function show($accountNumber, Request $request)
    {
        $request->request->add(['account_number' => $accountNumber]);

        $validatedRequest = $request->validate([
            'account_number' => [
                'required',
                'regex:/^[0-9]{26}$/',
                'exists:accounts,number,deleted_at,NULL'
            ]
        ]);

        $account = Account::find($validatedRequest['account_number']);

        return ['balance' => $account->balance];
    }
}
