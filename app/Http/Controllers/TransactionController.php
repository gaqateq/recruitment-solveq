<?php

namespace App\Http\Controllers;

use App\Transaction;
use Illuminate\Http\Request;

class TransactionController extends Controller
{
    public function show($accountNumber, Request $request)
    {
        $request->request->add(['account_number' => $accountNumber]);

        $validatedRequest = $request->validate([
            'account_number' => [
                'required',
                'regex:/^[0-9]{26}$/',
                'exists:accounts,number,deleted_at,NULL'
            ]
        ]);

        return Transaction::where('source_account_number', $validatedRequest['account_number'])
            ->latest()
            ->get([
                'destination_account_number',
                'amount',
                'balance_after',
                'title',
                'created_at'
            ]);
    }
}
