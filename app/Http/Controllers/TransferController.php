<?php

namespace App\Http\Controllers;

use App\Account;
use App\Rules\EnoughFunds;
use App\Transaction;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class TransferController extends Controller
{
    public function store(Request $request)
    {
        $validatedRequest = $request->validate([
            'source_account_number'      => [
                'required',
                'regex:/^[0-9]{26}$/',
                'different:destination_account_number',
                'exists:accounts,number,deleted_at,NULL'
            ],
            'destination_account_number' => [
                'required',
                'regex:/^[0-9]{26}$/',
                'different:source_account_number',
                'exists:accounts,number,deleted_at,NULL'
            ],
            'amount'                     => [
                'required',
                'numeric',
                'between:0.01,999999999999.99',
                new EnoughFunds($request->get('source_account_number', ''))
            ],
            'title'                      => [
                'required',
                'string',
                'min:2',
                'max:140'
            ]
        ]);

        DB::transaction(function () use ($validatedRequest) {
            $sourceAccount = Account::find($validatedRequest['source_account_number']);
            $destinationAccount = Account::find($validatedRequest['destination_account_number']);

            $transaction = new Transaction();
            $transaction->source_account_number = $validatedRequest['source_account_number'];
            $transaction->destination_account_number = $validatedRequest['destination_account_number'];
            $transaction->amount = round($validatedRequest['amount'] * -1, 2);
            $transaction->title = $validatedRequest['title'];
            $transaction->balance_after = round($sourceAccount->balance - $validatedRequest['amount'], 2);
            $transaction->save();

            $transaction = new Transaction();
            $transaction->source_account_number = $validatedRequest['destination_account_number'];
            $transaction->destination_account_number = $validatedRequest['source_account_number'];
            $transaction->amount = round($validatedRequest['amount'], 2);
            $transaction->title = $validatedRequest['title'];
            $transaction->balance_after = round($destinationAccount->balance + $validatedRequest['amount'], 2);
            $transaction->save();
        }, 2);
    }
}
