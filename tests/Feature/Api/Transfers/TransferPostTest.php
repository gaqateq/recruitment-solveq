<?php

namespace Tests\Feature\Api\Transfers;

use App\Account;
use App\Transaction;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class TransferPostTest extends TestCase
{
    use RefreshDatabase;

    public function testRequiredParameters()
    {
        $response = $this->withHeader('Accept', 'application/json')
            ->post('/api/transfers');

        $response->assertStatus(422);
        $response->assertJsonValidationErrors([
            'source_account_number',
            'destination_account_number',
            'amount',
            'title'
        ]);
    }

    /**
     * @dataProvider invalidSourceAccountNumberDataProvider
     */
    public function testInvalidSourceAccountNumber($accountNumber)
    {
        $response = $this->withHeader('Accept', 'application/json')
            ->post('/api/transfers', [
                'source_account_number' => $accountNumber
            ]);

        $response->assertStatus(422);
        $response->assertJsonValidationErrors(['source_account_number']);
    }

    public function invalidSourceAccountNumberDataProvider()
    {
        return [
            ['1234567890123456789012345'],
            ['123456789012345678901234567']
        ];
    }

    public function testNonExistingSourceAccountNumber()
    {
        $response = $this->withHeader('Accept', 'application/json')
            ->post('/api/transfers', [
                'source_account_number' => '12345678901234567890123456'
            ]);

        $response->assertStatus(422);
        $response->assertJsonValidationErrors(['source_account_number']);
    }

    public function testDeletedSourceAccountNumber()
    {
        $sourceAccount = factory(Account::class)->create();
        $sourceAccount->delete();

        $response = $this->withHeader('Accept', 'application/json')
            ->post('/api/transfers', [
                'source_account_number' => $sourceAccount->number
            ]);

        $response->assertStatus(422);
        $response->assertJsonValidationErrors(['source_account_number']);
    }

    /**
     * @dataProvider invalidDestinationAccountNumberDataProvider
     */
    public function testInvalidDestinationAccountNumber($accountNumber)
    {
        $response = $this->withHeader('Accept', 'application/json')
            ->post('/api/transfers', [
                'destination_account_number' => $accountNumber
            ]);

        $response->assertStatus(422);
        $response->assertJsonValidationErrors(['destination_account_number']);
    }

    public function invalidDestinationAccountNumberDataProvider()
    {
        return [
            ['1234567890123456789012345'],
            ['123456789012345678901234567']
        ];
    }

    public function testNonExistingDestinationAccountNumber()
    {
        $response = $this->withHeader('Accept', 'application/json')
            ->post('/api/transfers', [
                'destination_account_number' => '12345678901234567890123456'
            ]);

        $response->assertStatus(422);
        $response->assertJsonValidationErrors(['destination_account_number']);
    }

    public function testDeletedDestinationAccountNumber()
    {
        $destinationAccount = factory(Account::class)->create();
        $destinationAccount->delete();

        $response = $this->withHeader('Accept', 'application/json')
            ->post('/api/transfers', [
                'destination_account_number' => $destinationAccount->number
            ]);

        $response->assertStatus(422);
        $response->assertJsonValidationErrors(['destination_account_number']);
    }

    public function testEqualSourceAccountNumberAndDestinationAccountNumber()
    {
        $account = factory(Account::class)->create();

        $response = $this->withHeader('Accept', 'application/json')
            ->post('/api/transfers', [
                'source_account_number'      => $account->number,
                'destination_account_number' => $account->number
            ]);

        $response->assertStatus(422);
        $response->assertJsonValidationErrors(['source_account_number', 'destination_account_number']);
    }

    /**
     * @dataProvider invalidAmountDataProvider
     */
    public function testInvalidAmount($amount)
    {
        $sourceAccount = factory(Account::class)->create();

        factory(Transaction::class)->create([
            'source_account_number'      => $sourceAccount->number,
            'destination_account_number' => null,
            'amount'                     => 999999999999.99,
            'balance_after'              => 999999999999.99,
            'title'                      => ''
        ]);

        $response = $this->withHeader('Accept', 'application/json')
            ->post('/api/transfers', [
                'source_account_number' => $sourceAccount->number,
                'amount'                => $amount
            ]);

        $response->assertStatus(422);
        $response->assertJsonValidationErrors(['amount']);
    }

    public function invalidAmountDataProvider()
    {
        return [
            [''],
            ['foo'],
            ['1foo'],
            ['foo1'],
            [0],
            [0.001],
            [0.009],
            [1000000000000]
        ];
    }

    public function testNotEnoughFundsWithoutTransactions()
    {
        $sourceAccount = factory(Account::class)->create();

        $response = $this->withHeader('Accept', 'application/json')
            ->post('/api/transfers', [
                'source_account_number' => $sourceAccount->number,
                'amount'                => 10
            ]);

        $response->assertStatus(422);
        $response->assertJsonValidationErrors(['amount']);
    }

    public function testNotEnoughFundsWithTransactions()
    {
        $amount = 9.99;

        $sourceAccount = factory(Account::class)->create();

        factory(Transaction::class)->create([
            'source_account_number'      => $sourceAccount->number,
            'destination_account_number' => null,
            'amount'                     => $amount,
            'balance_after'              => $amount,
            'title'                      => ''
        ]);

        factory(Transaction::class)->create([
            'source_account_number' => $sourceAccount->number,
            'amount'                => round($amount * -1, 2),
            'balance_after'         => 0,
            'title'                 => 'transfer'
        ]);

        $response = $this->withHeader('Accept', 'application/json')
            ->post('/api/transfers', [
                'source_account_number' => $sourceAccount->number,
                'amount'                => $amount
            ]);

        $response->assertStatus(422);
        $response->assertJsonValidationErrors(['amount']);
    }

    /**
     * @dataProvider invalidTitleDataProvider
     */
    public function testInvalidTitle($title)
    {
        $response = $this->withHeader('Accept', 'application/json')
            ->post('/api/transfers', [
                'title' => $title
            ]);

        $response->assertStatus(422);
        $response->assertJsonValidationErrors(['title']);
    }

    public function invalidTitleDataProvider()
    {
        return [
            [''],
            ['f'],
            [substr(base64_encode(random_bytes(150)), 0, 141)]
        ];
    }

    public function testValidRequest()
    {
        $amount = 10;
        $amount2 = 9.99;

        $title = 'transfer 1';
        $title2 = 'transfer 2';

        $sourceAccount = factory(Account::class)->create();
        $destinationAccount = factory(Account::class)->create();

        factory(Transaction::class)->create([
            'source_account_number'      => $sourceAccount->number,
            'destination_account_number' => null,
            'amount'                     => $amount,
            'balance_after'              => $amount,
            'title'                      => ''
        ]);

        factory(Transaction::class)->create([
            'source_account_number'      => $sourceAccount->number,
            'destination_account_number' => null,
            'amount'                     => $amount2,
            'balance_after'              => round($amount + $amount2, 2),
            'title'                      => ''
        ]);

        $response = $this->withHeader('Accept', 'application/json')
            ->post('/api/transfers', [
                'source_account_number'      => $sourceAccount->number,
                'destination_account_number' => $destinationAccount->number,
                'amount'                     => $amount,
                'title'                      => $title,
            ]);

        $response->assertStatus(200);
        $this->assertSame('', $response->getContent());

        $this->assertDatabaseHas('transactions', [
            'source_account_number'      => $sourceAccount->number,
            'destination_account_number' => $destinationAccount->number,
            'amount'                     => round($amount * -1, 2),
            'title'                      => $title,
            'balance_after'              => $amount2,
        ]);

        $this->assertDatabaseHas('transactions', [
            'source_account_number'      => $destinationAccount->number,
            'destination_account_number' => $sourceAccount->number,
            'amount'                     => $amount,
            'title'                      => $title,
            'balance_after'              => $amount,
        ]);

        $response = $this->withHeader('Accept', 'application/json')
            ->post('/api/transfers', [
                'source_account_number'      => $sourceAccount->number,
                'destination_account_number' => $destinationAccount->number,
                'amount'                     => $amount2,
                'title'                      => $title2,
            ]);

        $response->assertStatus(200);
        $this->assertSame('', $response->getContent());

        $this->assertDatabaseHas('transactions', [
            'source_account_number'      => $sourceAccount->number,
            'destination_account_number' => $destinationAccount->number,
            'amount'                     => round($amount2 * -1, 2),
            'title'                      => $title2,
            'balance_after'              => 0,
        ]);

        $this->assertDatabaseHas('transactions', [
            'source_account_number'      => $destinationAccount->number,
            'destination_account_number' => $sourceAccount->number,
            'amount'                     => $amount2,
            'title'                      => $title2,
            'balance_after'              => round($amount + $amount2, 2),
        ]);
    }
}
