<?php

namespace Tests\Feature\Api\Balances;

use App\Account;
use App\Transaction;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class BalanceGetTest extends TestCase
{
    use RefreshDatabase;

    /**
     * @dataProvider invalidAccountNumberDataProvider
     */
    public function testInvalidAccountNumber($accountNumber)
    {
        $response = $this->withHeader('Accept', 'application/json')
            ->get('/api/balances/' . $accountNumber);

        $response->assertStatus(422);
        $response->assertJsonValidationErrors(['account_number']);
    }

    public function invalidAccountNumberDataProvider()
    {
        return [
            ['1234567890123456789012345'],
            ['123456789012345678901234567']
        ];
    }

    public function testNonExistingAccountNumber()
    {
        $response = $this->withHeader('Accept', 'application/json')
            ->get('/api/balances/12345678901234567890123456');

        $response->assertStatus(422);
        $response->assertJsonValidationErrors(['account_number']);
    }

    public function testDeletedAccountNumber()
    {
        $account = factory(Account::class)->create();
        $account->delete();

        $response = $this->withHeader('Accept', 'application/json')
            ->get('/api/balances/' . $account->number);

        $response->assertStatus(422);
        $response->assertJsonValidationErrors(['account_number']);
    }

    public function testEmptyBalance()
    {
        $account = factory(Account::class)->create();

        $response = $this->withHeader('Accept', 'application/json')
            ->get('/api/balances/' . $account->number);

        $response->assertStatus(200);
        $response->assertJson(['balance' => 0]);
    }

    public function testNotEmptyBalance()
    {
        $account = factory(Account::class)->create();

        $transaction = factory(Transaction::class)->create([
            'source_account_number' => $account->number
        ]);

        $response = $this->withHeader('Accept', 'application/json')
            ->get('/api/balances/' . $account->number);

        $response->assertStatus(200);
        $response->assertJson(['balance' => $transaction->balance_after]);
    }
}
