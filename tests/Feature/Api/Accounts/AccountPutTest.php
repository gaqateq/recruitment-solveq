<?php

namespace Tests\Feature\Api\Accounts;

use App\Account;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class AccountPutTest extends TestCase
{
    use RefreshDatabase;

    public function testRequiredParametersAndNonExistingAccountNumber()
    {
        $response = $this->withHeader('Accept', 'application/json')
            ->put('/api/accounts/12345678901234567890123456');

        $response->assertStatus(422);
        $response->assertJsonValidationErrors(['account_number', 'first_name', 'last_name', 'email']);
    }

    /**
     * @dataProvider invalidAccountNumberDataProvider
     */
    public function testInvalidAccountNumber($accountNumber)
    {
        $response = $this->withHeader('Accept', 'application/json')
            ->put('/api/accounts/' . $accountNumber);

        $response->assertStatus(422);
        $response->assertJsonValidationErrors(['account_number']);
    }

    public function invalidAccountNumberDataProvider()
    {
        return [
            ['1234567890123456789012345'],
            ['123456789012345678901234567']
        ];
    }

    public function testDeletedAccountNumber()
    {
        $account = factory(Account::class)->create();
        $account->delete();

        $response = $this->withHeader('Accept', 'application/json')
            ->put('/api/accounts/' . $account->number);

        $response->assertStatus(422);
        $response->assertJsonValidationErrors(['account_number']);
    }

    /**
     * @dataProvider invalidFirstNameDataProvider
     */
    public function testInvalidFirstName($firstName)
    {
        $account = factory(Account::class)->create();

        $response = $this->withHeader('Accept', 'application/json')
            ->put('/api/accounts/' . $account->number, [
                'first_name' => $firstName
            ]);

        $response->assertStatus(422);
        $response->assertJsonValidationErrors(['first_name']);
    }

    public function invalidFirstNameDataProvider()
    {
        return [
            [''],
            ['f'],
            [substr(base64_encode(random_bytes(260)), 0, 256)]
        ];
    }

    /**
     * @dataProvider invalidLastNameDataProvider
     */
    public function testInvalidLastName($lastName)
    {
        $account = factory(Account::class)->create();

        $response = $this->withHeader('Accept', 'application/json')
            ->put('/api/accounts/' . $account->number, [
                'last_name' => $lastName
            ]);

        $response->assertStatus(422);
        $response->assertJsonValidationErrors(['last_name']);
    }

    public function invalidLastNameDataProvider()
    {
        return [
            [''],
            ['f'],
            [substr(base64_encode(random_bytes(260)), 0, 256)]
        ];
    }

    /**
     * @dataProvider invalidEmailDataProvider
     */
    public function testInvalidEmail($email)
    {
        $account = factory(Account::class)->create();

        $response = $this->withHeader('Accept', 'application/json')
            ->put('/api/accounts/' . $account->number, [
                'email' => $email
            ]);

        $response->assertStatus(422);
        $response->assertJsonValidationErrors(['email']);
    }

    public function invalidEmailDataProvider()
    {
        return [
            ['plainaddress'],
            ['#@%^%#$@#$@#.com'],
            ['@domain.com'],
            ['Joe Smith <email@domain.com>'],
            ['email.domain.com'],
            ['email@domain@domain.com'],
            ['.email@domain.com'],
            ['email.@domain.com'],
            ['email..email@domain.com'],
            ['あいうえお@domain.com'],
            ['email@domain.com (Joe Smith)'],
            ['email@domain'],
            ['email@-domain.com'],
            ['email@111.222.333.44444'],
            ['email@domain..com'],
            [str_repeat('e', 245) . '@domain.com']
        ];
    }

    public function testValidRequest()
    {
        $account = factory(Account::class)->create();
        $firstName = 'foo';
        $lastName = 'bar';
        $email = 'email@domain.com';

        $response = $this->withHeader('Accept', 'application/json')
            ->put('/api/accounts/' . $account->number, [
                'first_name' => $firstName,
                'last_name'  => $lastName,
                'email'      => $email
            ]);

        $response->assertStatus(200);
        $this->assertSame('', $response->getContent());

        $this->assertDatabaseHas('accounts', [
            'number'     => $account->number,
            'first_name' => $firstName,
            'last_name'  => $lastName,
            'email'      => $email
        ]);
    }
}
