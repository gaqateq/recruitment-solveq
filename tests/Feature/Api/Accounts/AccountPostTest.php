<?php

namespace Tests\Feature\Api\Accounts;

use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class AccountPostTest extends TestCase
{
    use RefreshDatabase;

    public function testRequiredParameters()
    {
        $response = $this->withHeader('Accept', 'application/json')
            ->post('/api/accounts');

        $response->assertStatus(422);
        $response->assertJsonValidationErrors(['number', 'first_name', 'last_name', 'email']);
    }

    /**
     * @dataProvider invalidAccountNumberDataProvider
     */
    public function testInvalidAccountNumber($accountNumber)
    {
        $response = $this->withHeader('Accept', 'application/json')
            ->post('/api/accounts', [
                'number' => $accountNumber
            ]);

        $response->assertStatus(422);
        $response->assertJsonValidationErrors(['number']);
    }

    public function invalidAccountNumberDataProvider()
    {
        return [
            ['1234567890123456789012345'],
            ['123456789012345678901234567']
        ];
    }

    /**
     * @dataProvider invalidFirstNameDataProvider
     */
    public function testInvalidFirstName($firstName)
    {
        $response = $this->withHeader('Accept', 'application/json')
            ->post('/api/accounts', [
                'first_name' => $firstName
            ]);

        $response->assertStatus(422);
        $response->assertJsonValidationErrors(['first_name']);
    }

    public function invalidFirstNameDataProvider()
    {
        return [
            [''],
            ['f'],
            [substr(base64_encode(random_bytes(260)), 0, 256)]
        ];
    }

    /**
     * @dataProvider invalidLastNameDataProvider
     */
    public function testInvalidLastName($lastName)
    {
        $response = $this->withHeader('Accept', 'application/json')
            ->post('/api/accounts', [
                'last_name' => $lastName
            ]);

        $response->assertStatus(422);
        $response->assertJsonValidationErrors(['last_name']);
    }

    public function invalidLastNameDataProvider()
    {
        return [
            [''],
            ['f'],
            [substr(base64_encode(random_bytes(260)), 0, 256)]
        ];
    }

    /**
     * @dataProvider invalidEmailDataProvider
     */
    public function testInvalidEmail($email)
    {
        $response = $this->withHeader('Accept', 'application/json')
            ->post('/api/accounts', [
                'email' => $email
            ]);

        $response->assertStatus(422);
        $response->assertJsonValidationErrors(['email']);
    }

    public function invalidEmailDataProvider()
    {
        return [
            ['plainaddress'],
            ['#@%^%#$@#$@#.com'],
            ['@domain.com'],
            ['Joe Smith <email@domain.com>'],
            ['email.domain.com'],
            ['email@domain@domain.com'],
            ['.email@domain.com'],
            ['email.@domain.com'],
            ['email..email@domain.com'],
            ['あいうえお@domain.com'],
            ['email@domain.com (Joe Smith)'],
            ['email@domain'],
            ['email@-domain.com'],
            ['email@111.222.333.44444'],
            ['email@domain..com'],
            [str_repeat('e', 245) . '@domain.com']
        ];
    }

    public function testValidRequest()
    {
        $number = '12345678901234567890123456';
        $firstName = 'foo';
        $lastName = 'bar';
        $email = 'email@domain.com';

        $response = $this->withHeader('Accept', 'application/json')
            ->post('/api/accounts', [
                'number'     => $number,
                'first_name' => $firstName,
                'last_name'  => $lastName,
                'email'      => $email
            ]);

        $response->assertStatus(200);
        $this->assertSame('', $response->getContent());

        $this->assertDatabaseHas('accounts', [
            'number'     => $number,
            'first_name' => $firstName,
            'last_name'  => $lastName,
            'email'      => $email
        ]);
    }
}
