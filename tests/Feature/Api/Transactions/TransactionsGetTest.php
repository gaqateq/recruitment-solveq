<?php

namespace Tests\Feature\Api\Transactions;

use App\Account;
use App\Transaction;
use Illuminate\Support\Collection;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class TransactionsGetTest extends TestCase
{
    use RefreshDatabase;

    /**
     * @dataProvider invalidAccountNumberDataProvider
     */
    public function testInvalidAccountNumber($accountNumber)
    {
        $response = $this->withHeader('Accept', 'application/json')
            ->get('/api/transactions/' . $accountNumber);

        $response->assertStatus(422);
        $response->assertJsonValidationErrors(['account_number']);
    }

    public function invalidAccountNumberDataProvider()
    {
        return [
            ['1234567890123456789012345'],
            ['123456789012345678901234567']
        ];
    }

    public function testNonExistingAccountNumber()
    {
        $response = $this->withHeader('Accept', 'application/json')
            ->get('/api/transactions/12345678901234567890123456');

        $response->assertStatus(422);
        $response->assertJsonValidationErrors(['account_number']);
    }

    public function testDeletedAccountNumber()
    {
        $account = factory(Account::class)->create();
        $account->delete();

        $response = $this->withHeader('Accept', 'application/json')
            ->get('/api/transactions/' . $account->number);

        $response->assertStatus(422);
        $response->assertJsonValidationErrors(['account_number']);
    }

    public function testEmptyTransactions()
    {
        $account = factory(Account::class)->create();

        $response = $this->withHeader('Accept', 'application/json')
            ->get('/api/transactions/' . $account->number);

        $response->assertStatus(200);
        $response->assertJson([]);
    }

    public function testNotEmptyTransactions()
    {
        $account = factory(Account::class)->create();

        $transaction = factory(Transaction::class)->create([
            'source_account_number' => $account->number
        ]);

        $transaction2 = factory(Transaction::class)->create([
            'destination_account_number' => $account->number
        ]);

        $transaction3 = factory(Transaction::class)->create([
            'source_account_number'      => null,
            'destination_account_number' => $account->number
        ]);

        $transaction4 = factory(Transaction::class)->create([
            'source_account_number'      => $account->number,
            'destination_account_number' => null
        ]);

        $transaction->makeHidden(['source_account_number', 'updated_at', 'id']);
        $transaction4->makeHidden(['source_account_number', 'updated_at', 'id']);

        $collection = new Collection([$transaction, $transaction4]);

        $response = $this->withHeader('Accept', 'application/json')
            ->get('/api/transactions/' . $account->number);

        $response->assertStatus(200);
        $response->assertJson($collection->toArray());
    }
}
