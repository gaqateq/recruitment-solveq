<?php

namespace Tests\Feature\Api\Deposits;

use App\Account;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class DepositPostTest extends TestCase
{
    use RefreshDatabase;

    public function testRequiredParameters()
    {
        $response = $this->withHeader('Accept', 'application/json')
            ->post('/api/deposits');

        $response->assertStatus(422);
        $response->assertJsonValidationErrors(['account_number', 'amount']);
    }

    /**
     * @dataProvider invalidAccountNumberDataProvider
     */
    public function testInvalidAccountNumber($accountNumber)
    {
        $response = $this->withHeader('Accept', 'application/json')
            ->post('/api/deposits', [
                'account_number' => $accountNumber
            ]);

        $response->assertStatus(422);
        $response->assertJsonValidationErrors(['account_number']);
    }

    public function invalidAccountNumberDataProvider()
    {
        return [
            ['1234567890123456789012345'],
            ['123456789012345678901234567']
        ];
    }

    public function testNonExistingAccountNumber()
    {
        $response = $this->withHeader('Accept', 'application/json')
            ->post('/api/deposits', [
                'account_number' => '12345678901234567890123456'
            ]);

        $response->assertStatus(422);
        $response->assertJsonValidationErrors(['account_number']);
    }

    public function testDeletedAccountNumber()
    {
        $account = factory(Account::class)->create();
        $account->delete();

        $response = $this->withHeader('Accept', 'application/json')
            ->post('/api/deposits', [
                'account_number' => $account->number
            ]);

        $response->assertStatus(422);
        $response->assertJsonValidationErrors(['account_number']);
    }

    /**
     * @dataProvider invalidAmountDataProvider
     */
    public function testInvalidAmount($amount)
    {
        $account = factory(Account::class)->create();

        $response = $this->withHeader('Accept', 'application/json')
            ->post('/api/deposits', [
                'account_number' => $account->number,
                'amount'         => $amount
            ]);

        $response->assertStatus(422);
        $response->assertJsonValidationErrors(['amount']);
    }

    public function invalidAmountDataProvider()
    {
        return [
            [''],
            ['foo'],
            ['1foo'],
            ['foo1'],
            [0],
            [0.001],
            [0.009],
            [1000000000000]
        ];
    }

    public function testValidRequest()
    {
        $amount = 10;
        $amount2 = 9.99;

        $account = factory(Account::class)->create();

        $response = $this->withHeader('Accept', 'application/json')
            ->post('/api/deposits', [
                'account_number' => $account->number,
                'amount'         => $amount
            ]);

        $response->assertStatus(200);
        $this->assertSame('', $response->getContent());

        $this->assertDatabaseHas('transactions', [
            'source_account_number'      => $account->number,
            'destination_account_number' => null,
            'amount'                     => $amount,
            'title'                      => '',
            'balance_after'              => $amount,
        ]);

        $response = $this->withHeader('Accept', 'application/json')
            ->post('/api/deposits', [
                'account_number' => $account->number,
                'amount'         => $amount2
            ]);

        $response->assertStatus(200);
        $this->assertSame('', $response->getContent());

        $this->assertDatabaseHas('transactions', [
            'source_account_number'      => $account->number,
            'destination_account_number' => null,
            'amount'                     => $amount2,
            'title'                      => '',
            'balance_after'              => round($amount + $amount2, 2),
        ]);
    }
}
