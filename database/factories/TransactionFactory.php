<?php

use Faker\Generator as Faker;

$factory->define(App\Transaction::class, function (Faker $faker) {
    return [
        'source_account_number'      => $faker->iban('', '', 24),
        'destination_account_number' => $faker->iban('', '', 24),
        'amount'                     => $faker->randomFloat(2),
        'balance_after'              => $faker->randomFloat(2),
        'title'                      => $faker->text(140)
    ];
});
